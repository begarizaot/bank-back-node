"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
const generalRoutes_1 = __importDefault(require("./routes/generalRoutes"));
const usuarioRoutes_1 = __importDefault(require("./routes/usuarioRoutes"));
const cuentaRoutes_1 = __importDefault(require("./routes/cuentaRoutes"));
const transferenciaRoutes_1 = __importDefault(require("./routes/transferenciaRoutes"));
class Server {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.routes();
    }
    config() {
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
        this.app.set('port', process.env.PORT || 3000);
        this.app.use(morgan_1.default('dev'));
        this.app.use(cors_1.default());
        this.app.use(express_1.default.static('imagenes'));
        this.app.use(express_1.default.json({ limit: '50mb' }));
        this.app.use(express_1.default.urlencoded({ extended: false, limit: '50mb' }));
    }
    routes() {
        this.app.use('/api/general', generalRoutes_1.default);
        this.app.use('/api/usuario', usuarioRoutes_1.default);
        this.app.use('/api/cuenta', cuentaRoutes_1.default);
        this.app.use('/api/transferencias', transferenciaRoutes_1.default);
    }
    start() {
        this.app.listen(this.app.get('port'), () => {
            console.log(`Server on port ${this.app.get('port')}`);
        });
    }
}
const server = new Server();
server.start();
