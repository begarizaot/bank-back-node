"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const cuentaControllers_1 = require("../controllers/cuentaControllers");
class CuentaRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.post('/crearCuenta', cuentaControllers_1.cuentaControllers.postCrearCuenta);
        this.router.get('/cuentaUsuario/:id', cuentaControllers_1.cuentaControllers.getCuentasByIdUsuario);
        this.router.get('/cuentabyId/:id', cuentaControllers_1.cuentaControllers.getCuentasById);
    }
}
const cuentaRoutes = new CuentaRoutes();
exports.default = cuentaRoutes.router;
