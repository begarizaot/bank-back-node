"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const transferenciaControllers_1 = require("../controllers/transferenciaControllers");
class TransferenciaRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.post('/crearTransferancia', transferenciaControllers_1.transferenciaControllers.postCrearTrasferencia);
        this.router.get('/cuentaByIdCuenta/:idCuenta', transferenciaControllers_1.transferenciaControllers.getTrasferenciaByIdCuenta);
        this.router.get('/cuentaTransferenciaByCuenta/:cuenta', transferenciaControllers_1.transferenciaControllers.getTrasferenciaByCuenta);
    }
}
const transferenciaRoutes = new TransferenciaRoutes();
exports.default = transferenciaRoutes.router;
