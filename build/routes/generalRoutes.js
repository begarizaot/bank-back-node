"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const generalControllers_1 = require("../controllers/generalControllers");
class GeneralRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/tiposidentificacion', generalControllers_1.generalControllers.getTiposIdentificacion);
        this.router.get('/entidadesBancarias', generalControllers_1.generalControllers.getEntidadesBancarias);
        this.router.get('/tiposCuenta', generalControllers_1.generalControllers.getTiposCuenta);
        this.router.get('/monedas', generalControllers_1.generalControllers.getMonedas);
    }
}
const generalRoutes = new GeneralRoutes();
exports.default = generalRoutes.router;
