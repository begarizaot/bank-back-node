"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.cuentaControllers = void 0;
const database_1 = __importDefault(require("../database"));
class CuentaControllers {
    constructor() {
        this.postCrearCuenta = (req, res) => __awaiter(this, void 0, void 0, function* () {
            let { alias, entidadBanc: id_entidad, identificacion: titular, moneda: id_moneda, numCuenta: n_cuenta, tipoCuenta: id_cuenta, id_usuario, } = req.body;
            yield database_1.default
                .query(`SELECT * FROM cuentas WHERE n_cuenta = ?`, [n_cuenta])
                .then((resultado) => __awaiter(this, void 0, void 0, function* () {
                if (resultado.length > 0) {
                    const { alias } = resultado[0];
                    res.json({
                        messenger: `Lo siento, el numero de cuenta ya está en uso por ${alias}`,
                        state: true,
                    });
                }
                else {
                    yield database_1.default
                        .query(`INSERT INTO cuentas set ?`, [
                        {
                            alias,
                            id_entidad,
                            id_cuenta,
                            id_moneda,
                            n_cuenta,
                            titular,
                            id_usuario,
                        },
                    ])
                        .then((respuesta) => {
                        res.json({
                            state: false,
                            messenger: 'Se ha guardado exitosamente',
                        });
                    })
                        .catch((err) => {
                        res.json({
                            state: true,
                            messenger: 'Se presento un problema, por favor comunicarte con soporte para darle prioridad',
                            db: true,
                        });
                    });
                }
            }));
        });
        this.getCuentasByIdUsuario = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_1.default
                .query(`SELECT 
        cuentas.*, 
        entidad.label as entidad, 
        cuenta.label as cuenta,
        cuenta.icono,
        cuenta.borderColor,
        monedas.dim
        FROM cuentas 
        INNER JOIN entidadesbancarias as entidad ON cuentas.id_entidad = entidad.id
        INNER JOIN tiposcuenta as cuenta ON cuentas.id_cuenta = cuenta.id
        INNER JOIN monedas ON cuentas.id_moneda = monedas.id
        WHERE id_usuario = ?`, [id])
                .then((resultado) => __awaiter(this, void 0, void 0, function* () {
                yield database_1.default
                    .query(`SELECT documento FROM usuario WHERE id = ?`, [id])
                    .then((user) => {
                    const { documento } = user[0];
                    const data = {
                        propias: resultado.filter((inf) => inf.titular == documento),
                        tercero: resultado.filter((inf) => inf.titular != documento),
                    };
                    res.json(data);
                });
            }))
                .catch((err) => {
                res.json(err);
            });
        });
        this.getCuentasById = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_1.default
                .query(`SELECT 
        cuentas.*, 
        entidad.label as entidad, 
        cuenta.label as cuenta,
        cuenta.icono,
        cuenta.borderColor,
        monedas.dim
        FROM cuentas 
        INNER JOIN entidadesbancarias as entidad ON cuentas.id_entidad = entidad.id
        INNER JOIN tiposcuenta as cuenta ON cuentas.id_cuenta = cuenta.id
        INNER JOIN monedas ON cuentas.id_moneda = monedas.id
        WHERE cuentas.id = ?`, [id])
                .then((resultado) => __awaiter(this, void 0, void 0, function* () {
                const data = resultado[0];
                res.json(data);
            }))
                .catch((err) => {
                res.json(err);
            });
        });
    }
}
exports.cuentaControllers = new CuentaControllers();
