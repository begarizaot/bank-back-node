"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.transferenciaControllers = void 0;
const database_1 = __importDefault(require("../database"));
class TransferenciaControllers {
    constructor() {
        this.postCrearTrasferencia = (req, res) => __awaiter(this, void 0, void 0, function* () {
            let { id_usuario, id_origen, id_destino, id_moneda, valorTraf, cuenta, descripcion, saldo_origen, saldo_destino, } = req.body;
            yield database_1.default
                .query(`INSERT INTO transferencias set ?`, [
                {
                    id_usuario,
                    id_origen,
                    id_destino,
                    id_moneda,
                    valorTraf,
                    cuenta,
                    descripcion,
                },
            ])
                .then(({ insertId }) => __awaiter(this, void 0, void 0, function* () {
                const valorOrigen = Number(saldo_origen - valorTraf);
                const valorDestino = Number(saldo_destino + valorTraf);
                yield database_1.default
                    .query(`UPDATE cuentas SET saldo = ? WHERE id = ?`, [
                    valorOrigen,
                    id_origen,
                ])
                    .catch((err) => __awaiter(this, void 0, void 0, function* () {
                    yield database_1.default.query(`DELETE FROM transferencias WHERE id;`, [insertId]);
                }));
                yield database_1.default
                    .query(`UPDATE cuentas SET saldo = ? WHERE id = ?`, [
                    valorDestino,
                    id_destino,
                ])
                    .catch((err) => __awaiter(this, void 0, void 0, function* () {
                    yield database_1.default.query(`DELETE FROM transferencias WHERE id;`, [insertId]);
                }));
                res.json({
                    state: false,
                    messenger: 'Se ha guardado exitosamente',
                });
            }))
                .catch((err) => {
                res.json({
                    state: true,
                    messenger: 'Se presento un problema, por favor comunicarte con soporte para darle prioridad',
                    db: true,
                });
            });
        });
        this.getTrasferenciaByCuenta = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { cuenta } = req.params;
            yield database_1.default
                .query(`SELECT 
      transferencias.* ,
      origen.alias as aliasOrigen,
      destino.alias as aliasDestino,
      monedas.dim
      FROM transferencias
      INNER JOIN cuentas as origen ON transferencias.id_origen = origen.id
      INNER JOIN cuentas as destino ON transferencias.id_destino = destino.id
      INNER JOIN monedas ON transferencias.id_moneda = monedas.id
      WHERE cuenta = ?
      ORDER BY transferencias.id DESC`, [cuenta])
                .then((resultado) => {
                res.json(resultado);
            })
                .catch((err) => {
                res.json(err);
            });
        });
        this.getTrasferenciaByIdCuenta = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { idCuenta } = req.params;
            yield database_1.default
                .query(`SELECT 
      transferencias.* ,
      origen.alias as aliasOrigen,
      destino.alias as aliasDestino,
      monedas.dim
      FROM transferencias
      INNER JOIN cuentas as origen ON transferencias.id_origen = origen.id
      INNER JOIN cuentas as destino ON transferencias.id_destino = destino.id
      INNER JOIN monedas ON transferencias.id_moneda = monedas.id
      WHERE id_origen = ? or id_destino = ?
      ORDER BY transferencias.id DESC`, [idCuenta, idCuenta])
                .then((resultado) => {
                res.json(resultado);
            })
                .catch((err) => {
                res.json(err);
            });
        });
    }
}
exports.transferenciaControllers = new TransferenciaControllers();
