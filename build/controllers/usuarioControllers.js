"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.usuarioControllers = void 0;
const bcrypt_1 = __importDefault(require("bcrypt"));
const database_1 = __importDefault(require("../database"));
class UsuarioControllers {
    constructor() {
        this.postLoginUsuario = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { documento, password } = req.body;
            yield database_1.default
                .query(`SELECT * FROM usuario WHERE documento = ?`, [documento])
                .then((resultado) => {
                if (resultado.length > 0) {
                    let data = resultado[0];
                    bcrypt_1.default.compare(password, data.password).then(function (result) {
                        if (result) {
                            delete (data.password);
                            res.json({
                                state: false,
                                data,
                            });
                        }
                        else {
                            res.json({
                                messenger: 'No se encontró información del usuario',
                                state: true,
                            });
                        }
                    });
                }
                else {
                    res.json({
                        messenger: 'No se encontró información del usuario',
                        state: true,
                    });
                }
            })
                .catch((err) => {
                res.json({
                    state: true,
                    messenger: 'Se presento un problema, por favor comunicarte con soporte para darle prioridad',
                    db: true,
                });
            });
        });
        this.postCrearUsuario = (req, res) => __awaiter(this, void 0, void 0, function* () {
            let { documento, password } = req.body;
            yield database_1.default
                .query(`SELECT * FROM usuario WHERE documento = ?`, [documento])
                .then((resultado) => {
                if (resultado.length > 0) {
                    res.json({
                        messenger: 'Lo siento, el numero de identificación ya está en uso',
                        state: true,
                    });
                }
                else {
                    bcrypt_1.default.hash(password, 10).then((hash) => __awaiter(this, void 0, void 0, function* () {
                        password = hash;
                        database_1.default.query(`INSERT INTO usuario set ?`, [{ documento, password }])
                            .then(({ insertId }) => {
                            res.json({
                                state: false,
                                data: { id: insertId, documento, estado: 1 },
                            });
                        })
                            .catch((err) => {
                            res.json({
                                state: true,
                                messenger: 'Se presento un problema, por favor comunicarte con soporte para darle prioridad',
                                db: true,
                            });
                        });
                    }));
                }
            })
                .catch((err) => {
                res.json(err);
            });
        });
    }
}
exports.usuarioControllers = new UsuarioControllers();
