import express, { Application } from 'express';
import morgan from 'morgan';
import cors from 'cors';

import generalRoutes from './routes/generalRoutes';
import usuarioRoutes from './routes/usuarioRoutes';
import cuentaRoutes from './routes/cuentaRoutes';
import transferenciaRoutes from './routes/transferenciaRoutes';

class Server {
  public app: Application;

  constructor() {
    this.app = express();

    this.config();
    this.routes();
  }

  config(): void {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
    this.app.set('port', process.env.PORT || 3000);
    this.app.use(morgan('dev'));
    this.app.use(cors());
    this.app.use(express.static('imagenes'));
    this.app.use(express.json({ limit: '50mb' }));
    this.app.use(express.urlencoded({ extended: false, limit: '50mb' }));
  }

  routes(): void {
    this.app.use('/api/general', generalRoutes);
    this.app.use('/api/usuario', usuarioRoutes);
    this.app.use('/api/cuenta', cuentaRoutes);
    this.app.use('/api/transferencias', transferenciaRoutes);
  }

  start(): void {
    this.app.listen(this.app.get('port'), () => {
      console.log(`Server on port ${this.app.get('port')}`);
    });
  }
}

const server = new Server();

server.start();
