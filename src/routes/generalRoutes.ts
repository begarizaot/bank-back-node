import { Router } from 'express';
import { generalControllers } from '../controllers/generalControllers';

class GeneralRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  config(): void {
    this.router.get(
      '/tiposidentificacion',
      generalControllers.getTiposIdentificacion
    );

    this.router.get(
      '/entidadesBancarias',
      generalControllers.getEntidadesBancarias
    );

    this.router.get('/tiposCuenta', generalControllers.getTiposCuenta);
    
    this.router.get('/monedas', generalControllers.getMonedas);
  }
}

const generalRoutes = new GeneralRoutes();
export default generalRoutes.router;
