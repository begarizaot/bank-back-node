import { Router } from 'express';
import { usuarioControllers } from '../controllers/usuarioControllers';

class UsuarioRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  config(): void {
    this.router.post('/login', usuarioControllers.postLoginUsuario);
    this.router.post('/crearUsuario', usuarioControllers.postCrearUsuario);
  }
}

const usuarioRoutes = new UsuarioRoutes();
export default usuarioRoutes.router;
