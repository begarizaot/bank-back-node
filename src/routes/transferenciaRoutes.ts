import { Router } from 'express';
import { transferenciaControllers } from '../controllers/transferenciaControllers';

class TransferenciaRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  config(): void {
    this.router.post(
      '/crearTransferancia',
      transferenciaControllers.postCrearTrasferencia
    );
    this.router.get(
      '/cuentaByIdCuenta/:idCuenta',
      transferenciaControllers.getTrasferenciaByIdCuenta
    );
    this.router.get(
      '/cuentaTransferenciaByCuenta/:cuenta',
      transferenciaControllers.getTrasferenciaByCuenta
    );
  }
}

const transferenciaRoutes = new TransferenciaRoutes();
export default transferenciaRoutes.router;
