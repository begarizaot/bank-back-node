import { Router } from 'express';
import { cuentaControllers } from '../controllers/cuentaControllers';

class CuentaRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  config(): void {
    this.router.post('/crearCuenta', cuentaControllers.postCrearCuenta);
    this.router.get(
      '/cuentaUsuario/:id',
      cuentaControllers.getCuentasByIdUsuario
    );

    this.router.get('/cuentabyId/:id', cuentaControllers.getCuentasById);
  }
}

const cuentaRoutes = new CuentaRoutes();
export default cuentaRoutes.router;
