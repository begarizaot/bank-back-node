import { Request, Response } from 'express';

import db from '../database';

class GeneralControllers {
  public getTiposIdentificacion = async (req: Request, res: Response) => {
    await db
      .query(`SELECT * FROM tiposidentificacion`)
      .then((resultado) => {
        res.json(resultado);
      })
      .catch((err) => {
        res.json(err);
      });
  };

  public getEntidadesBancarias = async (req: Request, res: Response) => {
    await db
      .query(`SELECT * FROM entidadesBancarias`)
      .then((resultado) => {
        res.json(resultado);
      })
      .catch((err) => {
        res.json(err);
      });
  };

  public getTiposCuenta = async (req: Request, res: Response) => {
    await db
      .query(`SELECT * FROM tiposCuenta`)
      .then((resultado) => {
        res.json(resultado);
      })
      .catch((err) => {
        res.json(err);
      });
  };

  public getMonedas = async (req: Request, res: Response) => {
    await db
      .query(`SELECT * FROM monedas`)
      .then((resultado) => {
        res.json(resultado);
      })
      .catch((err) => {
        res.json(err);
      });
  };
}

export const generalControllers = new GeneralControllers();
