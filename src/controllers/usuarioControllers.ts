import { Request, Response } from 'express';
import bcrypt from 'bcrypt';

import db from '../database';

class UsuarioControllers {
  public postLoginUsuario = async (req: Request, res: Response) => {
    const { documento, password } = req.body;
    await db
      .query(`SELECT * FROM usuario WHERE documento = ?`, [documento])
      .then((resultado) => {
        if (resultado.length > 0) {
          let data = resultado[0];
          bcrypt.compare(password, data.password).then(function (result) {
            if (result) {
              delete(data.password)
              res.json({
                state: false,
                data,
              });
            } else {
              res.json({
                messenger: 'No se encontró información del usuario',
                state: true,
              });
            }
          });
        } else {
          res.json({
            messenger: 'No se encontró información del usuario',
            state: true,
          });
        }
      })
      .catch((err) => {
        res.json({
          state: true,
          messenger:
            'Se presento un problema, por favor comunicarte con soporte para darle prioridad',
          db: true,
        });
      });
  };

  public postCrearUsuario = async (req: Request, res: Response) => {
    let { documento, password } = req.body;

    await db
      .query(`SELECT * FROM usuario WHERE documento = ?`, [documento])
      .then((resultado) => {
        if (resultado.length > 0) {
          res.json({
            messenger: 'Lo siento, el numero de identificación ya está en uso',
            state: true,
          });
        } else {
          bcrypt.hash(password, 10).then(async (hash) => {
            password = hash;
            db.query(`INSERT INTO usuario set ?`, [{ documento, password }])
              .then(({ insertId }) => {
                res.json({
                  state: false,
                  data: { id: insertId, documento, estado: 1 },
                });
              })
              .catch((err) => {
                res.json({
                  state: true,
                  messenger:
                    'Se presento un problema, por favor comunicarte con soporte para darle prioridad',
                  db: true,
                });
              });
          });
        }
      })
      .catch((err) => {
        res.json(err);
      });
  };
}

export const usuarioControllers = new UsuarioControllers();
