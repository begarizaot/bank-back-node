import { Request, Response } from 'express';

import db from '../database';

class TransferenciaControllers {
  public postCrearTrasferencia = async (req: Request, res: Response) => {
    let {
      id_usuario,
      id_origen,
      id_destino,
      id_moneda,
      valorTraf,
      cuenta,
      descripcion,
      saldo_origen,
      saldo_destino,
    } = req.body;
    await db
      .query(`INSERT INTO transferencias set ?`, [
        {
          id_usuario,
          id_origen,
          id_destino,
          id_moneda,
          valorTraf,
          cuenta,
          descripcion,
        },
      ])
      .then(async ({ insertId }) => {
        const valorOrigen = Number(saldo_origen - valorTraf);
        const valorDestino = Number(saldo_destino + valorTraf);

        await db
          .query(`UPDATE cuentas SET saldo = ? WHERE id = ?`, [
            valorOrigen,
            id_origen,
          ])
          .catch(async (err) => {
            await db.query(`DELETE FROM transferencias WHERE id;`, [insertId]);
          });

        await db
          .query(`UPDATE cuentas SET saldo = ? WHERE id = ?`, [
            valorDestino,
            id_destino,
          ])
          .catch(async (err) => {
            await db.query(`DELETE FROM transferencias WHERE id;`, [insertId]);
          });

        res.json({
          state: false,
          messenger: 'Se ha guardado exitosamente',
        });
      })
      .catch((err) => {
        res.json({
          state: true,
          messenger:
            'Se presento un problema, por favor comunicarte con soporte para darle prioridad',
          db: true,
        });
      });
  };

  public getTrasferenciaByCuenta = async (req: Request, res: Response) => {
    const { cuenta } = req.params;
    await db
      .query(
        `SELECT 
      transferencias.* ,
      origen.alias as aliasOrigen,
      destino.alias as aliasDestino,
      monedas.dim
      FROM transferencias
      INNER JOIN cuentas as origen ON transferencias.id_origen = origen.id
      INNER JOIN cuentas as destino ON transferencias.id_destino = destino.id
      INNER JOIN monedas ON transferencias.id_moneda = monedas.id
      WHERE cuenta = ?
      ORDER BY transferencias.id DESC`,
        [cuenta]
      )
      .then((resultado) => {
        res.json(resultado);
      })
      .catch((err) => {
        res.json(err);
      });
  };

  public getTrasferenciaByIdCuenta = async (req: Request, res: Response) => {
    const { idCuenta } = req.params;
    await db
      .query(
        `SELECT 
      transferencias.* ,
      origen.alias as aliasOrigen,
      destino.alias as aliasDestino,
      monedas.dim
      FROM transferencias
      INNER JOIN cuentas as origen ON transferencias.id_origen = origen.id
      INNER JOIN cuentas as destino ON transferencias.id_destino = destino.id
      INNER JOIN monedas ON transferencias.id_moneda = monedas.id
      WHERE id_origen = ? or id_destino = ?
      ORDER BY transferencias.id DESC`,
        [idCuenta, idCuenta]
      )
      .then((resultado) => {
        res.json(resultado);
      })
      .catch((err) => {
        res.json(err);
      });
  };
}

export const transferenciaControllers = new TransferenciaControllers();
