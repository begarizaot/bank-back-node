import { Request, Response } from 'express';

import db from '../database';

class CuentaControllers {
  public postCrearCuenta = async (req: Request, res: Response) => {
    let {
      alias,
      entidadBanc: id_entidad,
      identificacion: titular,
      moneda: id_moneda,
      numCuenta: n_cuenta,
      tipoCuenta: id_cuenta,
      id_usuario,
    } = req.body;
    await db
      .query(`SELECT * FROM cuentas WHERE n_cuenta = ?`, [n_cuenta])
      .then(async (resultado) => {
        if (resultado.length > 0) {
          const { alias } = resultado[0];
          res.json({
            messenger: `Lo siento, el numero de cuenta ya está en uso por ${alias}`,
            state: true,
          });
        } else {
          await db
            .query(`INSERT INTO cuentas set ?`, [
              {
                alias,
                id_entidad,
                id_cuenta,
                id_moneda,
                n_cuenta,
                titular,
                id_usuario,
              },
            ])
            .then((respuesta) => {
              res.json({
                state: false,
                messenger: 'Se ha guardado exitosamente',
              });
            })
            .catch((err) => {
              res.json({
                state: true,
                messenger:
                  'Se presento un problema, por favor comunicarte con soporte para darle prioridad',
                db: true,
              });
            });
        }
      });
  };

  public getCuentasByIdUsuario = async (req: Request, res: Response) => {
    const { id } = req.params;
    await db
      .query(
        `SELECT 
        cuentas.*, 
        entidad.label as entidad, 
        cuenta.label as cuenta,
        cuenta.icono,
        cuenta.borderColor,
        monedas.dim
        FROM cuentas 
        INNER JOIN entidadesbancarias as entidad ON cuentas.id_entidad = entidad.id
        INNER JOIN tiposcuenta as cuenta ON cuentas.id_cuenta = cuenta.id
        INNER JOIN monedas ON cuentas.id_moneda = monedas.id
        WHERE id_usuario = ?`,
        [id]
      )
      .then(async (resultado) => {
        await db
          .query(`SELECT documento FROM usuario WHERE id = ?`, [id])
          .then((user) => {
            const { documento } = user[0];

            const data = {
              propias: resultado.filter((inf: any) => inf.titular == documento),
              tercero: resultado.filter((inf: any) => inf.titular != documento),
            };
            res.json(data);
          });
      })
      .catch((err) => {
        res.json(err);
      });
  };

  public getCuentasById = async (req: Request, res: Response) => {
    const { id } = req.params;
    await db
      .query(
        `SELECT 
        cuentas.*, 
        entidad.label as entidad, 
        cuenta.label as cuenta,
        cuenta.icono,
        cuenta.borderColor,
        monedas.dim
        FROM cuentas 
        INNER JOIN entidadesbancarias as entidad ON cuentas.id_entidad = entidad.id
        INNER JOIN tiposcuenta as cuenta ON cuentas.id_cuenta = cuenta.id
        INNER JOIN monedas ON cuentas.id_moneda = monedas.id
        WHERE cuentas.id = ?`,
        [id]
      )
      .then(async (resultado) => {
        const data = resultado[0]
        res.json(data);
      })
      .catch((err) => {
        res.json(err);
      });
  };
}

export const cuentaControllers = new CuentaControllers();
