-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-11-2021 a las 09:04:47
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bank`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

CREATE TABLE `cuentas` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `id_entidad` int(11) NOT NULL,
  `id_cuenta` int(11) NOT NULL,
  `id_moneda` int(11) NOT NULL,
  `n_cuenta` varchar(100) NOT NULL,
  `titular` varchar(100) NOT NULL,
  `saldo` int(30) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cuentas`
--

INSERT INTO `cuentas` (`id`, `id_usuario`, `alias`, `id_entidad`, `id_cuenta`, `id_moneda`, `n_cuenta`, `titular`, `saldo`) VALUES
(1, 1, 'bgarizao', 1, 1, 1, '12345678912', '1042448184', 300000),
(2, 1, 'egarizao', 1, 1, 1, '12345678913', '1042448184', 2000000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entidadesbancarias`
--

CREATE TABLE `entidadesbancarias` (
  `id` int(11) NOT NULL,
  `label` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `entidadesbancarias`
--

INSERT INTO `entidadesbancarias` (`id`, `label`, `estado`) VALUES
(1, 'Bancolombia', 1),
(2, 'Banco de Bogotá', 1),
(3, 'Davivienda', 1),
(4, 'BBVA', 1),
(5, 'Banco de Occidente', 1),
(6, 'Banco Itaú', 1),
(7, 'Banco Agrario', 1),
(8, 'Colpatria', 1),
(9, 'GNB Sudameris', 1),
(10, 'Banco Popular', 1),
(11, 'Banco Caja Social', 1),
(12, 'Banco AV Villas', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monedas`
--

CREATE TABLE `monedas` (
  `id` int(11) NOT NULL,
  `label` varchar(50) NOT NULL,
  `dim` varchar(5) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `monedas`
--

INSERT INTO `monedas` (`id`, `label`, `dim`, `estado`) VALUES
(1, 'Peso colombiano', 'COL', 1),
(2, 'Dólares', 'USD', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposcuenta`
--

CREATE TABLE `tiposcuenta` (
  `id` int(11) NOT NULL,
  `label` varchar(50) NOT NULL,
  `icono` varchar(20) NOT NULL,
  `borderColor` varchar(20) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tiposcuenta`
--

INSERT INTO `tiposcuenta` (`id`, `label`, `icono`, `borderColor`, `estado`) VALUES
(1, 'Cuenta de Ahorros', 'cash-outline', '#0140A9', 1),
(2, 'Cuenta Corriente', 'pulse-outline', '#ffc409', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiposidentificacion`
--

CREATE TABLE `tiposidentificacion` (
  `id` int(11) NOT NULL,
  `label` varchar(5) NOT NULL,
  `text` varchar(30) NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tiposidentificacion`
--

INSERT INTO `tiposidentificacion` (`id`, `label`, `text`, `estado`) VALUES
(1, 'C.C', 'Cédula de Ciudadanía', 1),
(2, 'C.E', 'Cédula de Extranjería', 1),
(3, 'P.S', 'Pasaporte', 1),
(4, 'T.I', 'Tajeta de Identidad', 1),
(5, 'R.C', 'Registro Civil', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transferencias`
--

CREATE TABLE `transferencias` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_origen` int(11) NOT NULL,
  `id_destino` int(11) NOT NULL,
  `id_moneda` int(11) NOT NULL,
  `valorTraf` int(30) NOT NULL,
  `cuenta` varchar(10) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `createDate` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `transferencias`
--

INSERT INTO `transferencias` (`id`, `id_usuario`, `id_origen`, `id_destino`, `id_moneda`, `valorTraf`, `cuenta`, `descripcion`, `createDate`) VALUES
(2, 1, 1, 2, 2, 100000, 'propia', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates harum tenetur molestias deleniti', '2021-11-08'),
(3, 1, 2, 1, 1, 50000, 'propia', 'lorem', '2021-11-08'),
(4, 1, 1, 2, 1, 50000, 'tercero', 'informacion', '2021-11-08'),
(5, 1, 2, 1, 1, 100000, 'propia', 'pago ', '2021-11-08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `documento` varchar(30) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `documento`, `nombre`, `password`, `estado`) VALUES
(1, '1042448184', 'Brayan', '$2b$10$v6uHYofIx9qxx4t/yO.URuGzlGczNB/TR1sOjM4LjRBLKooJeOZ9C', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `entidadesbancarias`
--
ALTER TABLE `entidadesbancarias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `monedas`
--
ALTER TABLE `monedas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tiposcuenta`
--
ALTER TABLE `tiposcuenta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tiposidentificacion`
--
ALTER TABLE `tiposidentificacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `transferencias`
--
ALTER TABLE `transferencias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `entidadesbancarias`
--
ALTER TABLE `entidadesbancarias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `monedas`
--
ALTER TABLE `monedas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tiposcuenta`
--
ALTER TABLE `tiposcuenta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tiposidentificacion`
--
ALTER TABLE `tiposidentificacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `transferencias`
--
ALTER TABLE `transferencias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
